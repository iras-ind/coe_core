#ifndef __ITIA_ROS_COE_UTILS__SDO__XMLRPC__H__
#define __ITIA_ROS_COE_UTILS__SDO__XMLRPC__H__

#include <mutex>
#include <yaml-cpp/yaml.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <XmlRpc.h>

#include <rosparam_utilities/rosparam_utilities.h>
#include <soem/ethercattype.h>
#include <coe_core/coe_string_utilities.h>
#include <coe_core/coe_pdo.h>
#include <coe_core/coe_sdo.h>

namespace coe_core
{ 

  namespace XmlRpcSdo
{

  static const char* KeysId[] = { "value_id"
                                , "value_index"
                                , "value_subindex"
                                , "value_type"
                                , "value_access"
                                , "value" };
                                
  enum KeysCode                 { VALUE_NAME
                                , VALUE_INDEX
                                , VALUE_SUBINDEX
                                , VALUE_TYPE
                                , VALUE_ACCESS
                                , VALUE };
                        
  
  inline void fromXmlRpcValue( const XmlRpc::XmlRpcValue& node, coe_core::Sdo& sdo, const std::string& log )
  {
    XmlRpc::XmlRpcValue config( node );
    if ( config.getType() != XmlRpc::XmlRpcValue::TypeArray )
    {
      ROS_ERROR ( "The node  is not of type array. %d/%d", int( config.getType()), int( XmlRpc::XmlRpcValue::TypeArray ) );
      throw std::runtime_error("The node  is not of type array");
    }
    
    sdo.clear();
    for(size_t i=0; i < size_t( config.size() ); i++) 
    {
      coe_core::BaseDataObjectEntryPtr obj;
      std::string   name            = rosparam_utilities::toString  ( config[i], KeysId[ VALUE_NAME     ], log + ", " + std::to_string(i)+"# name");
      int           value_index     = rosparam_utilities::toInt     ( config[i], KeysId[ VALUE_INDEX    ], log + ", " + std::to_string(i)+"# value_index"); 
      int           value_subindex  = rosparam_utilities::toInt     ( config[i], KeysId[ VALUE_SUBINDEX ], log + ", " + std::to_string(i)+"# value_subindex"); 
      std::string   value_type      = rosparam_utilities::toString  ( config[i], KeysId[ VALUE_TYPE     ], log + ", " + std::to_string(i)+"# value_type");
      std::string   value_access    = rosparam_utilities::toString  ( config[i], KeysId[ VALUE_ACCESS   ], log + ", " + std::to_string(i)+"# value_access");
      long int      value           = ( value_access == "write" ) 
                                    ? rosparam_utilities::toInt     ( config[i], KeysId[ VALUE ]         , log + ", " + std::to_string(i)+"# value") 
                                    : 0;
      
      switch( coe_core::getType( value_type )  )
      {
        case ECT_BOOLEAN:    obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_INTEGER8:   obj.reset( new coe_core::DataObjectEntry<int8_t     >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_INTEGER16:  obj.reset( new coe_core::DataObjectEntry<int16_t    >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_INTEGER32:  obj.reset( new coe_core::DataObjectEntry<int32_t    >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_INTEGER64:  obj.reset( new coe_core::DataObjectEntry<int64_t    >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_UNSIGNED8:  obj.reset( new coe_core::DataObjectEntry<uint8_t    >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_UNSIGNED16: obj.reset( new coe_core::DataObjectEntry<uint16_t   >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_UNSIGNED32: obj.reset( new coe_core::DataObjectEntry<uint32_t   >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_UNSIGNED64: obj.reset( new coe_core::DataObjectEntry<uint64_t   >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_REAL32:     obj.reset( new coe_core::DataObjectEntry<double     >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_REAL64:     obj.reset( new coe_core::DataObjectEntry<long double>( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_BIT1:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_BIT2:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_BIT3:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_BIT4:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_BIT5:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_BIT6:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_BIT7:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str(), value ) );   break;
        case ECT_BIT8:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str(), value ) );   break;
        default:             throw std::runtime_error("Type not yet implemented.");
      }
      sdo.push_back( obj );
      sdo.write_access[ obj->address() ]= ( value_access == "write" );
    }
  }
    
  inline void toXmlRpcValue( const coe_core::Sdo& sdo, XmlRpc::XmlRpcValue& xml_value  )
  {
    const size_t  npdo = sdo.nEntries(  );
    size_t        pdo_subindex = 0;
    xml_value.setSize( npdo );
    for( auto const & cob : sdo )
    {
      rosparam_utilities::toXmlRpcValue( cob->name()       , xml_value[ pdo_subindex ][ XmlRpcSdo::KeysId[ VALUE_NAME      ] ] );
      rosparam_utilities::toXmlRpcValue( cob->index()      , xml_value[ pdo_subindex ][ XmlRpcSdo::KeysId[ VALUE_INDEX     ] ], "hex" );
      rosparam_utilities::toXmlRpcValue( cob->subindex()   , xml_value[ pdo_subindex ][ XmlRpcSdo::KeysId[ VALUE_SUBINDEX  ] ] );
      rosparam_utilities::toXmlRpcValue( cob->type()       , xml_value[ pdo_subindex ][ XmlRpcSdo::KeysId[ VALUE_TYPE      ] ] );
    }
  }

};





                                              
  

}
#endif






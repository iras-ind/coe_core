/**
 *
 * @file coe_dictionary.h
 * @brief Namespace storing the structures to describe the dictionary according to CiA CANopen specifications
 *
 */

#ifndef __COE_SDO_DICTIONARY___H__
#define __COE_SDO_DICTIONARY___H__

#include <typeinfo>
#include <string>
#include <assert.h>
#include <exception>
#include <cstring>
#include <coe_core/coe_base.h>

namespace coe_core 
{

struct Sdo : public WeakDataObject 
{
  std::map< size_t, bool > write_access;
};

}

#endif

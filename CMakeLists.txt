cmake_minimum_required(VERSION 3.1)
project(coe_core)

set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

option( USE_SOEM              "Enable the SOEM support as Master Ethercat" ON )

if( USE_SOEM )
  set ( SOEM_DEFINITIONS "-D__USE_SOEM__=1 -Wall" )
  set ( SOEM_INCLUDE_DIRS "/usr/local/include/soem" )
endif()

find_package(catkin REQUIRED COMPONENTS cmake_modules realtime_utilities rosparam_utilities std_msgs roscpp realtime_tools message_generation)
find_package(Boost  REQUIRED program_options )

catkin_package(
   INCLUDE_DIRS include
   LIBRARIES coe_core
   CATKIN_DEPENDS roscpp realtime_tools std_msgs realtime_utilities
)

#######
#######
include_directories( include ${catkin_INCLUDE_DIRS} ${SOEM_INCLUDE_DIRS} ${XENOMAI_POSIX_INCLUDE_DIRS} )
add_definitions    ( ${SOEM_DEFINITIONS}  )
    

add_library(${PROJECT_NAME}
            src/${PROJECT_NAME}/ds301/coe_bitwise_struct.cpp
            src/${PROJECT_NAME}/ds301/coe_utilities.cpp    
            src/${PROJECT_NAME}/ds402/coe_xfsm_utilities.cpp      
            src/${PROJECT_NAME}/ds402/coe_bitwise_struct.cpp  
            src/${PROJECT_NAME}/coe_base.cpp
            src/${PROJECT_NAME}/coe_string_utilities.cpp
            src/${PROJECT_NAME}/coe_pdo.cpp
)
target_link_libraries(${PROJECT_NAME} ${Catkin_LIBRARIES} )
